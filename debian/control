Source: muttdown
Section: contrib/mail
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: gustavo panizzo <gfa@zumbi.com.ar>
Build-Depends:
  debhelper-compat (= 13),
  dh-python,
  python3-all,
  python3-setuptools,
  python3-pip,
  python3-pynliner,
  python3-markdown,
  python3-yaml,
  python3-pytest,
  python3,
  python3-six,
  python3-pytest-mock
Standards-Version: 4.6.0.1
Homepage: https://github.com/Roguelazer/muttdown
Vcs-Git: https://salsa.debian.org/python-team/packages/muttdown.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/muttdown
Rules-Requires-Root: no

Package: muttdown
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Recommends: ${python3:Recommends}, ${misc:Recommends}
Description: Compiles annotated text mail into html using the Markdown standard
 muttdown is a sendmail-replacement designed for use with the mutt email
 client which will transparently compile annotated text/plain mail into
 text/html using the Markdown standard.
 It will recursively walk the MIME tree and compile any text/plain or
 text/markdown part which begins with the sigil "!m" into Markdown,
 which it will insert alongside the original in a
 multipart/alternative container.
 .
 It's also smart enough not to break multipart/signed
